<script>
	var request = $.ajax({
    type: 'POST',
    url: 'http://localhost/tourdehamburg/api/packages.php?pkgName=daypkg',
    datatype : 'json' })
    .done(function(data) { 	

		plotMap(data);
		
		})
    .fail(function() { 
		alert("error"); 
		
		})
    .always(function() { 
		});



function plotMap(data)
{

	var d = jQuery.parseJSON(data);
	
	
	var mymap = L.map('mapid').setView([53.553189,10.006442], 15);

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11'
	}).addTo(mymap);

	L.marker([53.553189, 10.006442]).addTo(mymap).bindPopup("<b>"+d.packageName+"</b><br /> Central Station Hmaburg </br> walk starts from here ").openPopup();

	L.circle([53, -0.11], {
		color: 'red',
		fillColor: '#f03',
		fillOpacity: 0.5,
		radius: 5000
	}).addTo(mymap);


var myStr = d.jsonData.replace(/(\r\n|\n|\r)/gm, ""); //'[[53.553189,10.006442],[53.553154,10.006274,17],[53.553353,10.006162,17],[53.555269,10.002267,13],[53.555236,10.002323,13],[53.555099,10.002562,13.1],[53.554884,10.002936,15.3],[53.552893,10.005445,16.7],[53.553222,10.005452,17],[53.553291,10.005787,17],[53.553285,10.005842,17],[53.553353,10.006162,17],[53.553154,10.006274,17],[53.553189,10.006442,17]]';
var regex = /([^\[\],\s]+)/g;
var json = myStr.replace(regex, '"$&"');
var array = JSON.parse(json);



	L.polygon(array).addTo(mymap);

}

function saveData(){
	var firstName  = jQuery("#firstName").val();
	var lastName  = jQuery("#lastName").val();
	var email  = jQuery("#email").val();
	var passport  = jQuery("#passport").val();
	var flightnumber  = jQuery("#flightnumber").val();
	var packageId  = jQuery("#packageId").val();

	alert(packageId);
	
	var request = $.ajax({
    type: 'POST',
    url: 'http://localhost/tourdehamburg/api/usersData.php?firstName='+firstName+'&email='+email+'&passport='+passport+'&flightnumber='+flightnumber+'&lastName='+lastName+'&packageId='+packageId,
    datatype : 'json' })
    .done(function(data) { 	

		if(data){
			     jQuery("#msg").show(2000); 
		}
		
		})
    .fail(function() { 
		alert("error"); 
		
		})
    .always(function() { 
		});

	
}

</script>