<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>CodePen - leaflet map with place search</title>
  <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.css" />
<script src="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.js"></script>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet/0.0.1-beta.5/esri-leaflet.js"></script>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.css">
<link rel="stylesheet" href="styles/leafletCustomstyle.css">

</head>
<body>
<!-- partial:index.partial.html -->
<div id="map"></div>
<div class='pointer'>< Click search button</div>
<br>
message <input type="text" ><br>
locations <input type="text" id="lnglat" >

<!-- partial -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script  src="js/leafletCustomscript.js"></script>
  <script>
  var ownLocation ="["
  var lat =0;var lng =0;
  // Initialize the map and assign it to a variable for later use
var map = L.map('map', {
    // Set latitude and longitude of the map center (required)
    center: [37.7833, -122.4167],
    // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
    zoom: 10
});

L.control.scale().addTo(map);

// Create a Tile Layer and add it to the map
//var tiles = new L.tileLayer('http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png').addTo(map);
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  var searchControl = new L.esri.Controls.Geosearch().addTo(map);

  var results = new L.LayerGroup().addTo(map);

  searchControl.on('results', function(data){
	
	
    for (var i = data.results.length - 1; i >= 0; i--) {
      results.addLayer(L.marker(data.results[i].latlng));
	  
	 // console.log((data.results[i].latlng.lat) +" : "+ (data.results[i].latlng.lng));//custom array [[lat,lng],[lat,lng]]
	  lat =data.results[i].latlng.lat;
	  lng =data.results[i].latlng.lng;
	  ownLocation +="["+lat+","+lng+"],";
	  
	}
	document.getElementById("lnglat").value = ownLocation;
	console.log(ownLocation);
	
  });

setTimeout(function(){$('.pointer').fadeOut('slow');},3400);
  </script>

</body>
</html>
