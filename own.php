<!DOCTYPE html>

<html lang="en">
<head>
<title>TourDeHamburg</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Destino project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/elements_styles.css">
<link rel="stylesheet" type="text/css" href="styles/.css">
<link rel="stylesheet" type="text/css" href="styles/elements_responsive.css">
<link rel="stylesheet" type="text/css" href="styles/package1.css">
<link rel="stylesheet" type="text/css" href="styles/contact_styles.css">
<link rel="stylesheet" type="text/css" href="styles/contact_responsive.css">
<link href="https://fonts.googleapis.com/css?family=Handlee|Open+Sans:300,400,600,700,800" rel="stylesheet">
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>

<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.css" />
<script src="http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.js"></script>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet/0.0.1-beta.5/esri-leaflet.js"></script>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.css">
<link rel="stylesheet" href="styles/leafletCustomstyle.css">

<body>

<div class="super_container">
	
	<!-- Header -->

	<header class="header" style="z-index: 1">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center justify-content-start">

						<!-- Logo -->
						<div class="logo_container">
							<div class="logo">
								<div>Tour</div>
								<div>deHamburg</div>
								<div class="logo_image"><img src="images/logo.png" alt=""></div>
							</div>
						</div>

						<!-- Main Navigation --> 
			<ul class="main_nav_list">
			<nav class="main_nav ml-auto">
			<li class="main_nav_item active"><a href="index.html">Home</a></li>

    <li class="main_nav_item"><a href="packages.php">Packages</a></li>

    <li class="main_nav_item"><a href="explore.html">Explore Hamburg</a></li>

    <li class="main_nav_item"><a href="accommodate.html">To whom we accommodate</a></li>

    <li class="main_nav_item"><a href="contact.html">Who we are</a></li>

			</ul>
			</nav>

						<!-- Search -->
						<div class="search">
							<form action="#" class="search_form">
								<input type="search" name="search_input" class="search_input ctrl_class" required="required" placeholder="Keyword">
								<button type="submit" class="search_button ml-auto ctrl_class"><img src="images/search.png" alt=""></button>
							</form>
						</div>

						<!-- Hamburger -->
						<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>

					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Menu -->

	<div class="menu_container menu_mm">

		<!-- Menu Close Button -->
		<div class="menu_close_container">
			<div class="menu_close"></div>
		</div>

		<!-- Menu Items -->
		<div class="menu_inner menu_mm">
			<div class="menu menu_mm">
				<div class="menu_search_form_container">
					<form action="#" id="menu_search_form">
						<input type="search" class="menu_search_input menu_mm">
						<button id="menu_search_submit" class="menu_search_submit" type="submit"><img src="images/search_2.png" alt=""></button>
					</form>
				</div>
				<ul class="menu_list menu_mm">
					<li class="menu_item menu_mm"><a href="index.html">Home</a></li> 
					<li class="menu_item menu_mm"><a href="packages.php">Packages</a></li> 
					<li class="menu_item menu_mm"><a href="explore.html">Explore Hamburg</a></li> 
					<li class="menu_item menu_mm"><a href="accommodate.html">To whom we accommodate</a></li> 
					<li class="menu_item menu_mm"><a href="contact.html">Who we are</a></li> 
				</ul>

				<!-- Menu Social -->
				
				<div class="menu_social_container menu_mm">
					<ul class="menu_social menu_mm">
						<li class="menu_social_item menu_mm"><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
						<li class="menu_social_item menu_mm"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<li class="menu_social_item menu_mm"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li class="menu_social_item menu_mm"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li class="menu_social_item menu_mm"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					</ul>
				</div>

				<div class="menu_copyright menu_mm">TourDeHamburg All rights reserved</div>
			</div>

		</div>

	</div>
	
	<!-- Home -->

	<div class="home">
		<!-- Image by https://unsplash.com/@peecho -->
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/own.jpg" data-speed="0.8" transparent ="0.5"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_content_inner">
							<div class="home_title">Create own package</div>
							<div class="home_breadcrumbs">
								<ul class="home_breadcrumbs_list">
									<li class="home_breadcrumb"><a href="index.html">Home</a></li>
									<li class="home_breadcrumb"><a href="packages.php">Packages</a></li>
									<li class="home_breadcrumb">Create</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>

	<!-- Elements -->
	<center>
	<div class="elements_title">
	</br>
	<h3>Select on the map and submit below</h3>
	</br>
	</div>
	
	
	
<div id="map" style="width: 1000px; height: 600px;" style="z-index: -1"></div>
<br>
</center>
<!-- partial -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script  src="js/leafletCustomscript.js"></script>
  <script>
  var ownLocation ="["
  var lat =0;var lng =0;
  // Initialize the map and assign it to a variable for later use
var map = L.map('map', {
    // Set latitude and longitude of the map center (required)
    center: [53.553369, 10.00641],  
    // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
    zoom:15
});

L.control.scale().addTo(map);

// Create a Tile Layer and add it to the map
//var tiles = new L.tileLayer('http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png').addTo(map);
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  var searchControl = new L.esri.Controls.Geosearch().addTo(map);

  var results = new L.LayerGroup().addTo(map);

  searchControl.on('results', function(data){
	
	
    for (var i = data.results.length - 1; i >= 0; i--) {
      results.addLayer(L.marker(data.results[i].latlng));
	  
	 // console.log((data.results[i].latlng.lat) +" : "+ (data.results[i].latlng.lng));//custom array [[lat,lng],[lat,lng]]
	  lat =data.results[i].latlng.lat;
	  lng =data.results[i].latlng.lng;
	  ownLocation +="["+lat+","+lng+"],";
	  
	}
	document.getElementById("lnglat").value = ownLocation;
	console.log(ownLocation);
	
  });

setTimeout(function(){$('.pointer').fadeOut('slow');},3400);
  
  
function saveData(){
	
	var firstName  = jQuery("#firstName").val();
	var lastName  = jQuery("#lastName").val();
	var email  = jQuery("#email").val();
	var passport  = jQuery("#passport").val();
	var flightnumber  = jQuery("#flightnumber").val();
	var packageId  = 0;
	var latlng  = jQuery("#lnglat").val().substring(0, jQuery("#lnglat").val().length - 1);
	latlng += "]";
	
	console.log('http://localhost/tourdehamburg/api/usersData.php?firstName='+firstName+'&email='+email+'&passport='+passport+'&flightnumber='+flightnumber+'&lastName='+lastName+'&packageId='+packageId+'&latlng='+(latlng));
	var request = $.ajax({
    type: 'POST',
    url: 'http://localhost/tourdehamburg/api/usersData.php?firstName='+firstName+'&email='+email+'&passport='+passport+'&flightnumber='+flightnumber+'&lastName='+lastName+'&package_Id='+packageId+'&latlng='+(latlng),
    datatype : 'json' })
    .done(function(data) { 	

		if(data){
			     jQuery("#msg").show(); 
				
		}
		jQuery("#ok_btn").show();
		})
    .fail(function() { 
		alert("error"); 
		
		})
    .always(function() { 
		});

	
}
  
  </script>

 </div>
</br></br>
<div>
<style>
	.msg{width: 200px;
    background-color: #33a233;
    color: white;
    padding: 2px 2px 2px 2px;
    border-radius: 4px;}
	</style>
	<center>
	         <div class="col-lg-7">
					<div class="contact_form_container">
						<form action="" id="contact_form" class="clearfix">
							<input type="text" class="contact_input contact_input_name" placeholder="First Name" name="firstName" id="firstName" required="required" data-error="First Name is required.">
							<input type="text" class="contact_input contact_input_name"  placeholder="Last Name" name="lastName" id="lastName" required="required" data-error="Last Name is required."> 
							<input type="text" class="contact_input contact_input_name"  placeholder="E-mail" name="email" id="email" required="required" data-error="E-mail is required.">  <br/><br/>				
							<button id="contact_send_btn" type="button" name="submitPackage" class="contact_send_btn trans_200" value="Submit" onclick="saveData()" >Send</button>
							<input type="hidden" id="lnglat" >
							<div>
							<div class="msg" id="msg" style="display:none" >Request for your custom tour has been submited successfully, we will contact you soon</div>
							</br></br>
							
							<button id="ok_btn" class="contact_send_btn trans_200" type="button" name="tohome" style="display:none" onclick="window.location='http://localhost/tourdehamburg/packages.php'">Ok</button>
							</div>
						</form>
						
					</div>
				</div>
				</div>
 </center>

		<!-- Milestones -->

		<div class="milestones">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="elements_title">Milestones</div>
					</div>
				</div>
				<div class="row milestones_container">
					
					<!-- Milestone -->
					<div class="col-lg-3 milestone_col">
						<div class="milestone text-center">
							<div class="milestone_icon"><img class="svg" src="images/milestone_1.svg" alt=""></div>
							<div class="milestone_counter" data-end-value="17">0</div>
							<div class="milestone_text">Mountains Climbed</div>
						</div>
					</div>

					<!-- Milestone -->
					<div class="col-lg-3 milestone_col">
						<div class="milestone text-center">
							<div class="milestone_icon"><img class="svg" src="images/milestone_2.svg" alt=""></div>
							<div class="milestone_counter" data-end-value="213">0</div>
							<div class="milestone_text">Beach Visited</div>
						</div>
					</div>

					<!-- Milestone -->
					<div class="col-lg-3 milestone_col">
						<div class="milestone text-center">
							<div class="milestone_icon"><img class="svg" src="images/milestone_3.svg" alt=""></div>
							<div class="milestone_counter" data-end-value="11923">0</div>
							<div class="milestone_text">Photos Taken</div>
						</div>
					</div>

					<!-- Milestone -->
					<div class="col-lg-3 milestone_col">
						<div class="milestone text-center">
							<div class="milestone_icon"><img class="svg" src="images/milestone_4.svg" alt=""></div>
							<div class="milestone_counter" data-end-value="15">0</div>
							<div class="milestone_text">Cruises Organized</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<!-- Icon Boxes -->

		<div class="icon_boxes">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="elements_title">Benefits</div>
					</div>
				</div>
				<div class="row icon_box_container">

					<!-- Icon Box -->
					<div class="col-lg-4 icon_box_col">
						<div class="icon_box">
							<div class="icon_box_image"><img src="images/service_1.svg" class="svg" alt="https://www.flaticon.com/authors/monkik"></div>
							<div class="icon_box_title">Weekend solo transit trips. </div>
							<p class="icon_box_text">Traveling on weekend then do not waste your time in waiting at Airport gate just book a free tour and make your weekend more excited.</p>
							<a href="#" class="icon_box_more"></a>
						</div>
					</div>

					<!-- Icon Box -->
					<div class="col-lg-4 icon_box_col">
						<div class="icon_box">
							<div class="icon_box_image"><img src="images/service_2.svg" class="svg" alt="https://www.flaticon.com/authors/monkik"></div>
							<div class="icon_box_title">Fun leisure trip in transit. </div>
							<p class="icon_box_text">Traveling on weekend then do not waste your time in waiting at Airport gate just book a free tour and make your weekend more excited.</p>
							<a href="#" class="icon_box_more"></a>
						</div>
					</div>

					<!-- Icon Box -->
					<div class="col-lg-4 icon_box_col">
						<div class="icon_box">
							<div class="icon_box_image"><img src="images/service_3.svg" class="svg" alt="https://www.flaticon.com/authors/monkik"></div>
							<div class="icon_box_title">Extra with Plane tickets. </div>
							<p class="icon_box_text">Traveling on weekend then do not waste your time in waiting at Airport gate just book a free tour and make your weekend more excited.</p>
							<a href="#" class="icon_box_more"></a>
						</div>
					</div>

				</div>
			</div>
		</div>
			
	</div>

	<!-- Footer -->

<footer class="footer"> 
	<div class="container"> 
	<div class="row"> 
	
	<!-- Footer Column --> 
	<div class="col-lg-4 footer_col"> 
	<div class="footer_about"> 
	<!-- Logo --> 
	<div class="logo_container"> 
	<div class="logo"> 
	<div>Tour</div> 
	<div>De Hamburg</div> 
	<div class="logo_image"><img src="images/logo.pngg" alt=""></div> 

	</div> 
	</div> 
	<div class="footer_about_text">Giving a refreshing node to the people transiting in Hamburg to relax their mind</div> 
	 
	</div> 
	</div> 
	
	<!-- Footer Column --> 
	<div class="col-lg-4 footer_col"> 
	<div class="footer_latest"> 
	<div class="footer_title">Latest News</div> 
	<div class="footer_latest_content"> 
	
	<!-- Footer Latest Post --> 
	<div class="footer_latest_item"> 
	<div class="footer_latest_image"><img src="images/latest_1.jpg" alt="https://unsplash.com/@peecho"></div> 
	<div class="footer_latest_item_content"> 
	<div class="footer_latest_item_title"><a href="">Hamburg Summer</a></div> 
	<div class="footer_latest_item_date">Jan 09, 2020</div> 
	</div> 
	</div> 
	
	<!-- Footer Latest Post --> 
	<div class="footer_latest_item"> 
	<div class="footer_latest_image"><img src="images/latest_2.jpg" alt="https://unsplash.com/@sanfrancisco"></div> 
	<div class="footer_latest_item_content"> 
	<div class="footer_latest_item_title"><a href="">A perfect vacation</a></div> 
	<div class="footer_latest_item_date">Jan 09, 2020</div> 
	</div> 
	</div> 
	
	</div> 
	</div> 
	</div> 
	
	<!-- Footer Column --> 
	<div class="col-lg-4 footer_col"> 
	<div class="tags footer_tags"> 
	<div class="footer_title">Tags</div> 
	<ul class="tags_content d-flex flex-row flex-wrap align-items-start justify-content-start"> 
	<li class="tag"><a href="#">travel</a></li> 

	<li class="tag"><a href="#">summer</a></li> 

	<li class="tag"><a href="#">cruise</a></li> 

	<li class="tag"><a href="#">beach</a></li> 

	<li class="tag"><a href="#">offer</a></li> 

	<li class="tag"><a href="#">vacation</a></li> 

	<li class="tag"><a href="#">trip</a></li> 

	<li class="tag"><a href="#">city break</a></li> 

	<li class="tag"><a href="#">adventure</a></li> 

	</ul> 
	</div> 
	</div> 
	
	</div> 
	</div> 
	</footer> 
	</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/progressbar/progressbar.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/elements_custom.js"></script>

 
  <script src="js/jquery.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/bootstrap.js"></script>

  <script src="js/modernizr.custom.js"></script>
  <script src="js/toucheffects.js"></script>
  <script src="js/google-code-prettify/prettify.js"></script>
  <script src="js/jquery.bxslider.min.js"></script>
  <script src="js/camera/camera.js"></script>
  <script src="js/camera/setting.js"></script>

  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/portfolio/jquery.quicksand.js"></script>
  <script src="js/portfolio/setting.js"></script>

  <script src="js/jquery.flexslider.js"></script>
  <script src="js/animate.js"></script>
  <script src="js/inview.js"></script>

  <!-- Template Custom JavaScript File -->
  <script src="js/custom.js"></script>
</body>
</html>








